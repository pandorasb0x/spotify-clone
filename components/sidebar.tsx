import NextImage from 'next/image'
import { Box, List, ListItem, Divider, Center } from '@chakra-ui/layout'
import {
  MdHome,
  MdSearch,
  MdLibraryMusic,
  MdPlaylistAdd,
  MdFavorite,
} from 'react-icons/md'
import MenuItem from './menuItem'
import { usePlaylist } from '../lib/hoooks'

const navMenu = [
  { name: 'Home', icon: MdHome, route: '/' },
  { name: 'Search', icon: MdSearch, route: '/search' },
  { name: 'Your Library', icon: MdLibraryMusic, route: '/library' },
]

const musicMenu = [
  { name: 'Create Playlist', icon: MdPlaylistAdd, route: '/' },
  { name: 'Favourites', icon: MdFavorite, route: '/' },
]

const Sidebar = () => {
  const { playlists } = usePlaylist()

  return (
    <Box
      width="100%"
      height="calc(100vh - 100px)"
      bg="black"
      paddingX="5px"
      color="gray"
    >
      <Box paddingY="20px" height="100%">
        <Box width="120px" marginBottom="20px" paddingX="20px">
          <NextImage src="/logo.svg" height={60} width={120} />
        </Box>
        <Box marginBottom="20px">
          <List spacing={2}>
            {navMenu.map((menu) => (
              <ListItem paddingX="20px" fontSize="16px" key={menu.name}>
                <MenuItem
                  name={menu.name}
                  icon={menu.icon}
                  route={menu.route}
                />
              </ListItem>
            ))}
          </List>
        </Box>
        <Box marginTop="20px">
          <List spacing={2}>
            {musicMenu.map((menu) => (
              <ListItem paddingX="20px" fontSize="16px" key={menu.name}>
                <MenuItem
                  name={menu.name}
                  icon={menu.icon}
                  route={menu.route}
                />
              </ListItem>
            ))}
          </List>
        </Box>
        <Divider color="gray.800" />
        <Box
          height="66%"
          paddingY="20px"
          overflowY="auto"
          sx={{
            '&::-webkit-scrollbar': {
              w: '12px',
            },
            '&::-webkit-scrollbar-thumb': {
              bg: 'gray.600',
            },
          }}
        >
          <List spacing={2}>
            {playlists.map((playlist) => (
              <ListItem paddingX="20px" key={playlist.id}>
                <MenuItem
                  name={playlist.name}
                  route={{
                    pathname: '/playlist/[id]',
                    query: { id: playlist.id },
                  }}
                />
              </ListItem>
            ))}
          </List>
        </Box>
      </Box>
    </Box>
  )
}

export default Sidebar
