import NextLink from 'next/link'
import { LinkBox, LinkOverlay, ListIcon } from '@chakra-ui/layout'
import { IconType } from 'react-icons'

interface Route {
  pathname: string
  query: { id: string }
}
interface Props {
  name: string
  icon?: IconType
  route: Route | string
}

const MenuItem = ({ name, icon, route }: Props) => (
  <LinkBox>
    <NextLink href={route} passHref>
      <LinkOverlay>
        {icon && <ListIcon as={icon} color="white" marginRight="20px" />}
        {name}
      </LinkOverlay>
    </NextLink>
  </LinkBox>
)

MenuItem.defaultProps = {
  icon: null,
}

export default MenuItem
