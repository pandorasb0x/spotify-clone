import { Box, Flex, Text, Center } from '@chakra-ui/layout'
import { Image } from '@chakra-ui/react'
import GradientLayout from '../components/gradientLayout'
import { useMe } from '../lib/hoooks'
import prisma from '../lib/prisma'

const Home = ({ artists }) => {
  const { user, isLoading } = useMe()

  return (
    <GradientLayout
      color="cyan"
      subtitle="profile"
      title={`${user?.firstName} ${user?.lastName}`}
      isLoading={isLoading}
      description={`${user?.playlistsCount} public playlists`}
      image="./UserAvatar.jpg"
      roundImage
    >
      <Box color="white" paddingX="40px">
        <Box>
          <Text fontSize="2xl" fontWeight="bold">
            Top artist this month
          </Text>
          <Text fontSize="md">only visible to you</Text>
        </Box>
        <Flex>
          {artists.map((artist) => (
            <Box paddingX="10px" width="15%">
              <Box
                bg="gray.900"
                borderRadius="4px"
                padding="18px"
                width="100%"
                height="350px"
              >
                <Center>
                  <Image
                    src="./ArtistDefaultImage.jpg"
                    borderRadius="100%"
                    width="200px"
                  />
                </Center>

                <Box marginTop="20px">
                  <Text fontSize="large" fontWeight="bold">
                    {artist.name}
                  </Text>
                  <Text marginTop="5px" fontSize="md" color="gray.500">
                    Artist
                  </Text>
                </Box>
              </Box>
            </Box>
          ))}
        </Flex>
      </Box>
    </GradientLayout>
  )
}

export const getServerSideProps = async () => {
  const artists = await prisma.artist.findMany({})

  return {
    props: { artists },
  }
}
export default Home
